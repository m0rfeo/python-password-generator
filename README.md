# Python Password Generator 

This script generate random password based on all possible characters with length defined by the user and copy the password into clipboard

## Usage

```
$ python3 password-generator.py
$ Length of password: 12
$ YOUR PASSWORD HAS BEEN COPIED TO THE CLIPBOARD
CRTL+V to paste password
```

## License

Like all my projects this program is licensed under GPL 3.0 License. You can do anything that you want with my work except change license type, I will be graeful if you mention me.

## Misc

Author: m0rfeo

Open to suggestions. :)
