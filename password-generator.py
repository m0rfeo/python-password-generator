#!/usr/bin/env python
import random     # Module for random functions
import string     # Module to generate strings and characters
import array      # Module for arrays
import pyperclip  # Module to copy password to clipboard (Installation required)

print ("Python Password Generator - m0rfeo")
length = int(input("Length of password: "))

# All available characters to generate password
dictionary = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ1234567890!|#$%&/()=?'¡¿ç}*+[]^{-_.:;,@" 

# Generate random password with length indicated
random_array = [random.choice(dictionary) for i in range(length)]   # Choice one position of the string until the array have same elements that length defined
password = "".join(random_array)                                    # Convert array previously generated to an string, removing all unnecessary characters

# Copy password string to clipboard
pyperclip.copy(password)
print("YOUR PASSWORD HAS BEEN COPIED TO THE CLIPBOARD")


